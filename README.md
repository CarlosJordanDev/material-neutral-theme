# Material Neutral Theme
### For Visual Studio Code

![screenshot](http://i.imgur.com/MQRRWam.jpg)

### For more information
* [Gitlab](https://gitlab.com/bernardodsanderson/material-neutral-theme)

**Enjoy!**